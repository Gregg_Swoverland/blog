class Post < ActiveRecord::Base
  # add comment associations and destroy comments when post is deleted
  has_many :comments, dependent: :destroy 
end
